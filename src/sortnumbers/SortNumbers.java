package sortnumbers;

/**
 * Programming for Industry - Introduction to Java
 * Sorting Numbers
 * 
 * Note: Put your answers in between the "// Answer here //" comments. Do not modify other parts of the class.
 */
public class SortNumbers {
	
	/**
	 * Sorting numbers in order.
	 */
	public void sortNumberByAscending(int number1, int number2, int number3, int number4) {
		int first = 0;
		int second = 0;
		int third = 0;
		int fourth = 0;
		// Answer here


		int minOf1And2 = Math.min(number1, number2);
		int minOf2And3 = Math.min(number3, number4);

		first = Math.min(minOf1And2, minOf2And3);

		int maxOf1And2 = Math.max(number1, number2);
		int maxOf3And4 = Math.max(number3, number4);

		second = Math.min(
				 	Math.min(
						maxOf1And2,
						Math.min(
							Math.max(number2, number3),
							maxOf3And4
						)
					),
					Math.min(
						Math.max(number4, number1),
						Math.min(
							Math.max(number2, number4),
							Math.max(number3, number1)
						)
					)
				);

		third = Math.max(
				Math.max(
						minOf1And2,
						Math.max(
								Math.min(number2, number3),
								minOf2And3
						)
				),
				Math.max(
						Math.min(number4, number1),
						Math.max(
								Math.min(number2, number4),
								Math.min(number3, number1)
						)
				)
		);

		fourth = Math.max(maxOf1And2, maxOf3And4);

		//
		System.out.println("The numbers are: " + first + ", " + second + ", " + third + ", " + fourth);
	}

	/**
	 * Don't edit this - but read/use this for testing if you like.
	 */
	public static void main(String[] args) {
		SortNumbers cr = new SortNumbers();
		
		cr.sortNumberByAscending(35, -4, 7, 6); // The numbers are: -4, 6, 7, 35
		cr.sortNumberByAscending(-1, 0, 18, -10); // The numbers are: -10, -1, 0, 18
		cr.sortNumberByAscending(1, 2, 3, 4); // The numbers are: 1, 2, 3, 4
	}

}
